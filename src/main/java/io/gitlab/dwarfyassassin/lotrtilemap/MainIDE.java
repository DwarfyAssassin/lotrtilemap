package io.gitlab.dwarfyassassin.lotrtilemap;

import java.awt.image.BufferedImage;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import io.gitlab.dwarfyassassin.lotrtilemap.tilemap.TilemapMaker;
import io.gitlab.dwarfyassassin.lotrtilemap.util.ImageUtil;
import io.gitlab.dwarfyassassin.lotrtilemap.util.MathUtil;

public class MainIDE {
	
	private static String pathToMap = ".\\tilemaps\\normalMap\\map.png"; //Path to the main map file
	private static String outputPath = ".\\test_tiles"; //Output folder for the tilemaps
	private static int tileSize = 512; //Tile size, needs to be a power of 2, usually 256 or 512
	private static int minZoom = 0; //Start making tile form this zoom level
	private static int maxZoom = 5; //Stop making tile form this zoom level
	
	private static boolean calcMaxZoom = false;

	public static void main(String[] args) throws InterruptedException {
		long time = System.currentTimeMillis();
		TilemapMaker mapper = new TilemapMaker();
		
		BufferedImage img = ImageUtil.getImage(pathToMap);
		if(img == null) {
			sendHelp("Invalid image path. A example is C:\\Users\\<User>\\Desktop\\map.png");
			return;
		}

		if(!MathUtil.isPowerOfTwo(tileSize)) {
			sendHelp("Invalid tile size. The tile size has to be a power of 2. Most commonly used are 256 for normal tiles and 512 for HD tiles.");
			return;
		}
			
		if(!calcMaxZoom) {
			try {
				Path outputPath = Paths.get(MainIDE.outputPath);
				System.out.println("Writing tiles to folder: " + outputPath.toAbsolutePath().normalize());
			}
			catch(InvalidPathException e) {
				sendHelp("Invalid output path.");
				return;
			}
			
			mapper.makeTileMap(img, outputPath, minZoom, maxZoom, tileSize);
			System.out.println("Done writing tiles. Time spend: " + (System.currentTimeMillis() - time) / 1000.0 + " seconds");
		}
		else {
			int maxZoom = mapper.getImageMaxZoom(img, tileSize);
			System.out.println("The theoretical max zoom for this image is " + maxZoom);
		}
	}
	
	private static void sendHelp(String error) {
		System.err.println(error);
		System.out.println("Invalid arguments, expected these arguments.");
		System.out.println("Expected arguments: <mapPath> <tileSize> [outputPath] [minZoom] [maxZoom]");
		System.out.println();
		System.out.println("Example: java -jar Tilemap.jar map.png 512 C:\\<User>\\desktop\\tiles 0 6");
	}
	
	public static void testing() {
		long time1 = 0;
		long time2 = 0;
		long time3 = 0;
		int times = 5000;
		

		int originalSize = 128;
		BufferedImage startImg = new BufferedImage(originalSize, originalSize, BufferedImage.TYPE_INT_ARGB);
    	
    	for(int x = 0; x < originalSize; x++) {
    		for(int y = 0; y < originalSize; y++) {
    			startImg.setRGB(x, y, 1);
        	}
    	}
		
		int zoomFactor = 512 / originalSize;
		

		System.out.println("Starting testing 1.");
		long startTime1 = System.currentTimeMillis();
		for(int i = 0; i < times; i++) testing1(startImg, zoomFactor);
    	time1 = System.currentTimeMillis() - startTime1;

		System.out.println("Starting testing 2.");
		long startTime2 = System.currentTimeMillis();
		for(int i = 0; i < times; i++) testing2(startImg, zoomFactor);
    	time2 = System.currentTimeMillis() - startTime2;

		System.out.println("Starting testing 3.");
		long startTime3 = System.currentTimeMillis();
		for(int i = 0; i < times; i++) testing3(startImg, zoomFactor);
    	time3 = System.currentTimeMillis() - startTime3;
		
		System.out.println("Testing 1: " + time1 + " - avg: " + (time1 / (double) times));
		System.out.println("Testing 2: " + time2 + " - avg: " + (time2 / (double) times));
		System.out.println("Testing 3: " + time3 + " - avg: " + (time3 / (double) times));
	}
	
	public static void testing1(BufferedImage startImg, int zoomFactor) {
		BufferedImage newImg1 = new BufferedImage(startImg.getWidth() * zoomFactor, startImg.getHeight() * zoomFactor, BufferedImage.TYPE_INT_ARGB);
    	
		for(int x1 = 0; x1 < startImg.getWidth(); x1++) {
			for(int y1 = 0; y1 < startImg.getHeight(); y1++) {
				int pixelColor = startImg.getRGB(x1, y1);
			
				for(int x2 = 0; x2 < zoomFactor; x2++) {
					for(int y2 = 0; y2 < zoomFactor; y2++) {
						newImg1.setRGB(x1*zoomFactor + x2, y1*zoomFactor + y2, pixelColor);
					}
				}
			}
		}
	}
	
	public static void testing2(BufferedImage startImg, int zoomFactor) {
		int [] intArr = new int[startImg.getWidth() * zoomFactor * startImg.getHeight() * zoomFactor];
		int strideSize = startImg.getWidth() * zoomFactor;
    	
		for(int x1 = 0; x1 < startImg.getWidth(); x1++) {
			for(int y1 = 0; y1 < startImg.getHeight(); y1++) {
				int pixelColor = startImg.getRGB(x1, y1);
				
				for(int x2 = 0; x2 < zoomFactor; x2++) {
					for(int y2 = 0; y2 < zoomFactor; y2++) {
						intArr[(y1*zoomFactor + y2) * strideSize + x1*zoomFactor + x2] = pixelColor;
					}
				}
			}
		}
		
    	BufferedImage img2 = new BufferedImage(startImg.getWidth() * zoomFactor, startImg.getHeight() * zoomFactor, BufferedImage.TYPE_INT_ARGB);
		img2.setRGB(0, 0, startImg.getWidth() * zoomFactor, startImg.getHeight() * zoomFactor, intArr, 0, strideSize);
	}
	
	public static void testing3(BufferedImage startImg, int zoomFactor) {
		int[] baseIntArr = startImg.getRGB(0, 0, startImg.getWidth(), startImg.getHeight(), null, 0, startImg.getWidth()); //new int[startImg.getWidth() * zoomFactor * startImg.getHeight() * zoomFactor];
    	int[] newIntArr = new int[baseIntArr.length * zoomFactor * zoomFactor];
    	int baseStrideSize = startImg.getWidth();
    	int newStrideSize = startImg.getWidth() * zoomFactor;
    	
    	for(int index = 0; index < baseIntArr.length; index++) {
    		int pixelColor = baseIntArr[index];
    		int x1 = index % baseStrideSize;
    		int y1 = index / baseStrideSize;
    		
    		for(int x2 = 0; x2 < zoomFactor; x2++) {
				for(int y2 = 0; y2 < zoomFactor; y2++) {
					newIntArr[(y1*zoomFactor + y2) * newStrideSize + x1*zoomFactor + x2] = pixelColor;
				}
			}
    	}
		
    	BufferedImage img3 = new BufferedImage(startImg.getWidth() * zoomFactor, startImg.getHeight() * zoomFactor, BufferedImage.TYPE_INT_ARGB);
    	img3.setRGB(0, 0, startImg.getWidth() * zoomFactor, startImg.getHeight() * zoomFactor, newIntArr, 0, newStrideSize);
	}
		
}
