package io.gitlab.dwarfyassassin.lotrtilemap.util;

public class MathUtil {
	
	public static boolean isPowerOfTwo(int i) {
		return i > 0 && (i & i - 1) == 0; // From https://www.programcreek.com/2014/07/leetcode-power-of-two-java/
	}
	
	public static int getPowerOfTwo(int i) {
		if(!isPowerOfTwo(i)) return -1;
		
		int power = 0;
		
		while(i != 1) {
			i >>= 1;
			power++;
		}
		
		return power;
	}

}
