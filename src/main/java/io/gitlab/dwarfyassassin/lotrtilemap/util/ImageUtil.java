package io.gitlab.dwarfyassassin.lotrtilemap.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ImageUtil {
	private static Map<Integer, BufferedImage> cachedImages = new HashMap<Integer, BufferedImage>();
	

	public static BufferedImage getImage(String imgPath) {
		try {
			return ImageIO.read(new File(imgPath));
		} catch (IOException e) {
			System.out.println(imgPath + " - This isn't a img.");
			return null;
		}
    }
	
	public static BufferedImage enlargeImg(BufferedImage img, int zoomFactor) {
    	int[] baseIntArr = img.getRGB(0, 0, img.getWidth(), img.getHeight(), null, 0, img.getWidth());
    	int[] newIntArr = new int[baseIntArr.length * zoomFactor * zoomFactor];
    	int baseStrideSize = img.getWidth();
    	int newStrideSize = img.getWidth() * zoomFactor;
    	
    	int color = baseIntArr[0];
    	boolean singleColor = true;
    	for(int pixelColor : baseIntArr) {
    		if(color != pixelColor) {
    			singleColor = false;
    			break;
    		}
    	}
    	
    	BufferedImage cachedImg = cachedImages.get(color);
    	if(singleColor && cachedImg != null) return cachedImg;
    	
    	for(int index = 0; index < baseIntArr.length; index++) {
    		int pixelColor = baseIntArr[index];
    		int x1 = index % baseStrideSize;
    		int y1 = index / baseStrideSize;
    		
    		for(int x2 = 0; x2 < zoomFactor; x2++) {
				for(int y2 = 0; y2 < zoomFactor; y2++) {
					newIntArr[(y1*zoomFactor + y2) * newStrideSize + x1*zoomFactor + x2] = pixelColor;
				}
			}
    	}
		
    	BufferedImage newImg = new BufferedImage(img.getWidth() * zoomFactor, img.getHeight() * zoomFactor, BufferedImage.TYPE_INT_ARGB);
    	newImg.setRGB(0, 0, img.getWidth() * zoomFactor, img.getHeight() * zoomFactor, newIntArr, 0, newStrideSize);
    	
    	if(singleColor) cachedImages.put(color, newImg);
    	
    	return newImg;
    }
	
	public static void writeImg(BufferedImage img, String stringPath, boolean verbose) {
    	File path = new File(stringPath);
		if(!path.exists()) path.mkdirs();

		try {
			ImageIO.write(img, "png", path);
			if(verbose) System.out.println("Succesfully wrote img to: " + stringPath);
		} catch (IOException e) {
			System.out.println("Failed to write img to: " + stringPath);
			e.printStackTrace();
		}
    }
}
