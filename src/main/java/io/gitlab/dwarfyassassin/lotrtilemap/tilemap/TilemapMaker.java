package io.gitlab.dwarfyassassin.lotrtilemap.tilemap;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import io.gitlab.dwarfyassassin.lotrtilemap.util.MathUtil;

public class TilemapMaker  {
	
	private TilemapWorker[] workers;
	private Queue<Work> workQueue;
	
	public int getImageMaxZoom(BufferedImage img, int tileSize) {
		if(isValidImg(img, tileSize)) return MathUtil.getPowerOfTwo(img.getWidth());
		
		return -1;
	}
	
	public boolean isValidImg(BufferedImage img, int tileSize) {
		int width = img.getWidth();
		int height = img.getHeight();
		
		if(width != height) {
			System.out.println("The image isn't valid as it isn't square!");
			return false;
		}
		if(!MathUtil.isPowerOfTwo(width)) {
			System.out.println("The image isn't valid as the width isn't a power of two.");
			return false;
		}
		if(width % tileSize != 0) {
			System.out.println("The image isn't valid as it can't fit a whole number of tiles of size " + tileSize);
			return false;
		}
		
		return true;
	}
	
	public void makeTileMap(BufferedImage img, String outputPath, int minZoom, int maxZoom, int tileSize) throws InterruptedException {
		if(!isValidImg(img, tileSize)) return;
		
		minZoom = Math.max(0, minZoom);
		maxZoom = Math.min(15, maxZoom);
		
		if(minZoom > maxZoom || maxZoom > getImageMaxZoom(img, tileSize)) {
			System.out.println("Invalid zoom levels.");
			return;
		}
		
		
		workers = new TilemapWorker[Runtime.getRuntime().availableProcessors() - 2];
		for(int i = 0; i < workers.length; i++) workers[i] = new TilemapWorker(this);
		
		System.out.println("Using " + workers.length + " threads.");

		workQueue = new Queue<Work>();
		for(int zoom = minZoom; zoom <= maxZoom; zoom++) {
			int amountPerSide = (int) Math.pow(2, zoom); // 16 for zoom level 4
			int inputTileSize = img.getWidth() / amountPerSide; // 256 for zoom level 4
			
			if(inputTileSize >= tileSize) {
				workQueue.add(new Work(zoom));
			}
			else {
				int workBatches = tileSize / inputTileSize; //2 for zoom level 4 (512 / 256)
				int tilesPerSize = amountPerSide / workBatches;
				
				for(int x = 0; x < workBatches; x++) {
					for(int y = 0; y < workBatches; y++) {
						workQueue.add(new Work(x * tilesPerSize, y * tilesPerSize, zoom));
					}
				}
			}
		}
		
		System.out.println("Total work amount: " + workQueue.remainingQueue());
		
		int currentZoom = -1;
		
		while(!workQueue.isDone() || areWorkersRunning()) {
			for(TilemapWorker worker : workers) {
				if(worker.isRunning() || workQueue.isDone()) continue;
				
				Work work = workQueue.getNext();

				int amountPerSide = (int) Math.pow(2, work.zoom); // 16 for zoom level 4
				int inputTileSize = img.getWidth() / amountPerSide; // 256 for zoom level 4
				int workBatches = tileSize / inputTileSize; //2 for zoom level 4 (512 / 256)
				
				BufferedImage subImg = img;
				
				if(workBatches > 1) {
					int offset = img.getWidth() / workBatches; // 2048 for zoom level 4
					subImg = img.getSubimage(work.x * inputTileSize, work.y * inputTileSize, offset, offset);
				}
				
				worker.runWorkerThread(subImg, inputTileSize, tileSize, work.getOutputPath(outputPath), work.x, work.y);
				
				if(currentZoom != work.zoom) {
					currentZoom = work.zoom;
					System.out.println("Starting on zoom level " + currentZoom);
				}
			}


	        Thread.sleep(500);
		}
		
    }
	
	public boolean areWorkersRunning() {
		for(TilemapWorker worker : workers) {
			if(worker.isRunning()) return true;
		}
		
		return false;
	}

    public static class Queue<E> {
    	private List<E> list;
    	private int index;
    	
    	public Queue() {
    		list = new ArrayList<E>();
    		index = 0;
    	}
    	
    	public void add(E element) {
    		list.add(element);
    	}
    	
    	public E getNext() {
    		if(isDone()) return null;
    		
    		return list.get(index++);
    	}
    	
    	public boolean isDone() {
    		return index >= list.size();
    	}
    	
    	public int remainingQueue() {
    		return list.size() - index;
    	}
    }
    
    public static class Work {
    	public int x;
    	public int y;
    	public int zoom;
    	

    	public Work(int zoom) {
    		this(0, 0, zoom);
    	}
    	
    	public Work(int x, int y, int zoom) {
    		this.x = x;
    		this.y = y;
    		this.zoom = zoom;
    	}
    	
    	public String getOutputPath(String outputFolderPath) {
    		return outputFolderPath + "\\" + zoom;
    	}
    }
}