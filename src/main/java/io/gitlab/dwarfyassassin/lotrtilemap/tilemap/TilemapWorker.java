package io.gitlab.dwarfyassassin.lotrtilemap.tilemap;

import java.awt.image.BufferedImage;

import io.gitlab.dwarfyassassin.lotrtilemap.util.ImageUtil;

public class TilemapWorker {
	
	private TilemapMaker parent;
	private boolean isRunning = false;
	
	public TilemapWorker(TilemapMaker parent) {
		this.parent = parent;
	}
	
	private void makeSubTiles(BufferedImage subImg, int inputTileSize, int outputTileSize, String outputPath, int startTilesX, int startTilesY) {
		int inputTileAmount = subImg.getWidth() / inputTileSize;
		
		for(int x = 0; x < inputTileAmount; x++) {
			for(int y = 0; y < inputTileAmount; y++) {
				BufferedImage tile = subImg.getSubimage(x * inputTileSize, y * inputTileSize, inputTileSize, inputTileSize);
			
				if(inputTileSize < outputTileSize) tile = ImageUtil.enlargeImg(tile, outputTileSize / inputTileSize);
				
				String path = outputPath + "\\" + (startTilesX + x) + "-" + (startTilesY + y) + ".png";
				ImageUtil.writeImg(tile, path, false);
			}
		}
	}
	
	public void runWorkerThread(BufferedImage subImg, int inputTileSize, int outputTileSize, String outputPath, int startX, int startY) {
        Thread thread = new Thread("Tilemap Worker") {

            @Override
            public void run() {
            	isRunning = true;
            	makeSubTiles(subImg, inputTileSize, outputTileSize, outputPath, startX, startY);
            	isRunning = false;
            }
        };
        
        thread.setDaemon(true);
        thread.start();
    }
	
	public boolean isRunning() {
		return isRunning;
	}

}
