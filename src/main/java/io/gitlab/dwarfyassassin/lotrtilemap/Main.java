package io.gitlab.dwarfyassassin.lotrtilemap;

import java.awt.image.BufferedImage;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import io.gitlab.dwarfyassassin.lotrtilemap.tilemap.TilemapMaker;
import io.gitlab.dwarfyassassin.lotrtilemap.util.ImageUtil;
import io.gitlab.dwarfyassassin.lotrtilemap.util.MathUtil;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		long time = System.currentTimeMillis();
		
		TilemapMaker mapper = new TilemapMaker();
		
		if(args.length >= 2) {
			BufferedImage img = ImageUtil.getImage(args[0]);
			if(img == null) {
				sendHelp("Invalid image path. A example is C:\\Users\\<User>\\Desktop\\map.png");
				return;
			}

			int tileSize;
			try {
				tileSize = Integer.valueOf(args[1]);
			}
			catch(NumberFormatException e) {
				sendHelp("Invalid tile size. The tile size has to be a power of 2. Most commonly used are 256 for normal tiles and 512 for HD tiles.");
				return;
			}
			if(!MathUtil.isPowerOfTwo(tileSize)) {
				sendHelp("Invalid tile size. The tile size has to be a power of 2. Most commonly used are 256 for normal tiles and 512 for HD tiles.");
				return;
			}
			
			if(args.length == 5) {
				
				try {
					Path outputPath = Paths.get(args[2]);
					System.out.println("Writing tiles to folder: " + outputPath.toAbsolutePath().normalize());
				}
				catch(InvalidPathException e) {
					sendHelp("Invalid output path.");
					return;
				}
				
				int minZoom;
				try {
					minZoom = Integer.valueOf(args[3]);
				}
				catch(NumberFormatException e) {
					sendHelp("Invalid min zoom.");
					return;
				}
				
				int maxZoom;
				try {
					maxZoom = Integer.valueOf(args[4]);
				}
				catch(NumberFormatException e) {
					sendHelp("Invalid max zoom.");
					return;
				}
				

				mapper.makeTileMap(img, args[2], minZoom, maxZoom, tileSize);
				System.out.println("Done writing tiles. Time spend: " + (System.currentTimeMillis() - time) / 1000.0 + " seconds");
			}
			else {
				int maxZoom = mapper.getImageMaxZoom(img, tileSize);
				System.out.println("The theoretical max zoom for this image is " + maxZoom);
			}
		}
		else {
			sendHelp();
		}
	}
	

	private static void sendHelp() {
		sendHelp("");
	}
	
	private static void sendHelp(String error) {
		System.err.println(error);
		System.out.println("Invalid arguments, expected these arguments.");
		System.out.println("Expected arguments: <mapPath> <tileSize> [outputPath] [minZoom] [maxZoom]");
		System.out.println();
		System.out.println("Example: java -jar Tilemap.jar map.png 512 C:\\<User>\\desktop\\tiles 0 6");
	}
}
