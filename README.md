###What is this

This is a public repo which anyone can use to make tile maps for the LOTR map. It contains the vanilla map and it also contains the territory map from [TROP](https://lotrminecraftmod.fandom.com/wiki/Servers/The_Ring_of_Power "TROP LOTR Wiki page.").

It also contains the java code used to generate these tile maps.

###How to use

This repo can be used by libraries like leaflet or mappa as tile map. The URL which you need to specify is `https://gitlab.com/DwarfyAssassin/lotrtilemap/raw/master/tilemaps/normalMap/{z}/{x}-{y}.png`.

The current maximum zoom level is 9.
It's also usefull if you specify the background color of the map to `#02598d` and set the map to no warp.

###Example

A example website can be found in [pages](https://dwarfyassassin.gitlab.io/lotrtilemap/ "Go to pages.") form this repository. The code for this can be found [here](https://gitlab.com/DwarfyAssassin/lotrtilemap/-/blob/master/public/index.html).

To run the script you can use the jar or run it from a IDE. If you plan on using the jar these are the needed arguments `java -jar Tilemap.jar <path to base image> <tile size> <path to output folder> <start zoom level> <end zoom level>`

So for example: `java -jar Tilemap.jar map.png 512 C:\\<User>\\desktop\\tiles 0 6`